"use strict";

$(document).ready(function() {


    //slick
    $(".slider").slick({
        arrows: true,
        dots: true,
        customPaging : function(slider, i) {
            var thumb = $(slider.$slides[i]).data('text');
            return '<a>'+thumb+'</a>';
        },
        responsive: [
            {
                breakpoint: 767,
                settings: {
                    arrows: false
                }
            }
        ]
    });
    //slick

    // phone
    $("[name=phone]").mask("+7 (999) 999-9999");
    // phone



    // anchor
    $(".anchor").on("click","a", function (event) {
        event.preventDefault();
        console.log('sss');
        var id  = $(this).attr('href'),
            top = $(id).offset().top;
        $('body,html').animate({scrollTop: top - 94}, 1000);
    });
    // anchor


    // modal
    $('.popup-close, .overlay, .close').click(function () {
        $('.modal')
            .animate({opacity: 0}, 200,
                function () {
                    $(this).css('display', 'none');
                    $('.overlay').fadeOut(400);
                }
            );
    });
    // modal



});